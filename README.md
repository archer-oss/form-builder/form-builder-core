# form-builder-core

Core logic for form builder (powered by the dependency checker).

## What is Form Builder?

Form Builder is a form state and layout management tool to make creating consistent and robust forms easier. Once a field or layout is defined by a unique `type`, it can be used in any form throughout the app. By using Form Builder, you can guarantee the expected behavior will be the same for all the forms in your app.

It follows the React philosophy of declarative style programming. You describe the behavior of your form and Form Builder takes care of the rest.

## Quick Start

### Install Form Builder

```bash
npm install @archer-oss/form-builder-core
```

### Configure Form Builder

While you don't have to use TypeScript to use Form Builder, it's recommended to improve developer experience. The rest of this quick start guide assumes the use of TypeScript.

#### Import Dependencies

```ts
import {
  configureFormBuilder,
  CreateDependenciesTypes,
  FBBaseFormFieldDependencyType,
  FBBaseLayoutFieldDependencyType,
  FormBuilderConfig,
} from '@archer-oss/form-builder-core';
```

#### Build Types

By using the Type creation utilities bundled with this package, you can ensure that you get proper TypeScript support.

```ts
// Form Fields are the input elements in your form.
type FormFields = {
  ['text-field']: { type: 'text-field'; props: { label: string }; value: string };
};

// Layout Fields are used to position input elements in the form.
// They don't have a value associated with them.
type LayoutFields = {
  ['two-column-layout']: { type: 'two-column-layout'; props: {} };
};

// Utility to ensure that all the combinations of
// dependencies are created for TypeScript support.
type Dependencies = CreateDependenciesTypes<FBBaseFormFieldDependencyType<FormFields>, FBBaseLayoutFieldDependencyType<LayoutFields>>;
```

#### Set Form Builder Defaults

When configuring Form Builder, you have the opportunity to set defaults for the fields to control what should be rendered and any properties that should have some kind of value.

```tsx
const config: FormBuilderConfig<FormFields, LayoutFields> = {
  defaultFormFieldConfigs: {
    'text-field': {
      data: {
        // If no label prop is passed when using a
        // text-field, this text will be used instead.
        props: { label: 'I am the default label' },
        // A function which gets the data, metaData and
        // setters for both to control the field's lifecycle
        render: ({ data, metaData, setData, setMetaData }) => ({
          <React.Fragment>
            <label>
              {data.props.label}
              <input
                type="text"
                value={data.value}
                onChange={e => {
                  // Optionally set the touched property so that the error below
                  // is only displayed when the user interacts with the field.
                  setMetaData({ touched: true });
                  setData({ value: e.target.value });
                }}
              />
            </label>
            {metaData.touched && data.value === '' && <span>Error: Empty Field</span>}
          </React.Fragment>
        })
      }
    }
  },
  defaultLayoutFieldConfigs: {
    'two-column-layout': {
      data: {
        // The fields that are created as part of
        // this group when using a two-column-layout
        render: ({ fields }) => {
          // Return a two column grid whenever this layout type is used.
          return (
            <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr' }}>
              {fields}
            </div>
          );
        }
      }
    }
  }
```

#### Create Form Builder Implementation

The `configureFormBuilder` function returns a unique implementation of Form Builder which has the default properties set and proper TypeScript support.

```ts
  const { FormBuilder } = configureFormBuilder<FormFields, LayoutFields, Dependencies>(config);
};
```

#### Use Form Builder

To use Form Builder, pass a set of props which includes an `onChange` which is invoked anytime a field updates in the form and an array of `groups`. The group types should match the LayoutField types. Each group should take an array of `fields`. These fields should match the FormFields.

```tsx
function MyForm() {
  const formBuilderProps = {
    onChange: console.log,
    groups: [
      {
        key: 'sign-up-info',
        // Setting this type allows you to
        // hook into the defaults from above.
        type: 'two-column-layout',
        fields: [
          {
            key: 'user-name',
            type: 'text-field',
            // Setting the label here overrides the default from above.
            data: { props: { label: 'User Name' } },
          },
          {
            key: 'email',
            type: 'text-field',
            data: { props: { label: 'Email' } },
          },
        ],
      },
    ],
  };

  return <FormBuilder {...formBuilderProps} />;
}
```
