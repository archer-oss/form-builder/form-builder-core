/* global document, window */
// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect';
import { configure } from '@testing-library/react';

/**
 * Configure React Testing Library
 * { defaultHidden: true }: speeds up *byRole queries - https://github.com/testing-library/dom-testing-library/issues/552
 */
configure({ testIdAttribute: 'data-selector', defaultHidden: true });

/**
 * Global Jest Hooks
 */
afterEach(() => {
  // Reset timers by default - https://github.com/testing-library/react-testing-library/pull/768#issuecomment-679198814
  jest.useRealTimers();
});
