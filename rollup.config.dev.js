import { createFileConfig } from '@archer-oss/dev-scripts/src/rollup.config';
import { DEFAULT_EXTENSIONS } from '@babel/core';
import { babel } from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';
import resolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';
import replace from '@rollup/plugin-replace';

const extensions = [...DEFAULT_EXTENSIONS, '.ts', '.tsx'];

export default createFileConfig({
  input: 'demo/index.tsx',
  plugins: [
    json(),
    replace({ preventAssignment: true, 'process.env.NODE_ENV': JSON.stringify('production') }),
    resolve({ extensions }),
    commonjs(),
    babel({ babelHelpers: 'runtime', extensions }),
    terser(),
  ],
  output: [{ file: `demo/index.esm.js`, format: 'esm' }],
});
