import { createFileConfig } from '@archer-oss/dev-scripts/src/rollup.config';

export default createFileConfig({
  input: 'src/index.tsx',
  external: ['react', /@babel\/runtime/],
});
