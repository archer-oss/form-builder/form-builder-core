import * as React from 'react';
import { setDependentKeyData, setDependentKeyMetaData, ResetStateRef } from '@archer-oss/dependency-checker-react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { FBDefaultFormField } from '../fields/fb-default-form-field';
import { FBDefaultLayoutField } from '../fields/fb-default-layout-field';
import { configureFormBuilder, FormBuilderConfig } from '../index';

const { FormBuilder } = generateFormBuiderConfig();
type FormBuilderProps = Parameters<typeof FormBuilder>[0];

describe('Form Builder Fields', () => {
  it.each([FBDefaultFormField, FBDefaultLayoutField])('Form Builder Field: %p throws an error if used outside of Form Builder', (Component) => {
    const consoleErrorMock = jest.spyOn(console, 'error').mockImplementation(() => {});
    expect(() => render(<Component fieldKey="test-key" baseData={{}} baseMetaData={{}} fields={[]} fieldMap={{}} />)).toThrow();
    expect(consoleErrorMock).toHaveBeenCalled();
    consoleErrorMock.mockRestore();
  });
});

describe('Form Builder', () => {
  it('should render the layout and field correctly with the default configurations', () => {
    const props: FormBuilderProps = {
      groups: [
        {
          key: 'group',
          type: 'layout-field',
          fields: [
            {
              key: 'field',
              type: 'text-field',
            },
          ],
        },
      ],
    };

    render(<FormBuilder {...props} />);

    expect(screen.getByText(/default layout title/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/default text field/i)).toBeInTheDocument();
  });

  it('should update fields based on dependencies', () => {
    const props: FormBuilderProps = {
      groups: [
        {
          key: 'group',
          type: 'layout-field',
          data: { props: { title: 'Custom Layout Title' } },
          dependencies: [
            {
              key: 'field',
              type: 'text-field',
              cond: (data) => data.value === 'enable dependency',
              effects: { props: { title: 'Dependency Layout Title' } },
            },
          ],
          fields: [
            {
              key: 'field',
              type: 'text-field',
              data: { props: { label: 'Custom Text Field' } },
              dependencies: [
                {
                  key: 'field',
                  type: 'text-field',
                  cond: (data) => data.value === 'enable dependency',
                  effects: { props: { label: 'Dependency Text Field' } },
                },
              ],
            },
          ],
        },
      ],
    };

    render(<FormBuilder {...props} />);

    expect(screen.getByText(/custom layout title/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/custom text field/i)).toBeInTheDocument();
    expect(screen.queryByText(/dependency layout title/i)).not.toBeInTheDocument();
    expect(screen.queryByLabelText(/dependency text field/i)).not.toBeInTheDocument();

    userEvent.paste(screen.getByLabelText(/custom text field/i), 'enable dependency');

    expect(screen.getByText(/dependency layout title/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/dependency text field/i)).toBeInTheDocument();
    expect(screen.queryByText(/custom layout title/i)).not.toBeInTheDocument();
    expect(screen.queryByLabelText(/custom text field/i)).not.toBeInTheDocument();
  });

  it.todo('should update fields based on a composite dependency correctly');

  it("should format a field's value in the onChange if a valueFormatter exists", () => {
    const onChange = jest.fn();

    const props: FormBuilderProps = {
      onChange,
      groups: [
        {
          key: 'group',
          type: 'layout-field',
          data: { props: { title: 'Custom Layout Title' } },
          fields: [
            {
              key: 'field',
              type: 'text-field',
              data: { props: { label: 'Text Field' } },
              metaData: { valueFormatter: (value) => value?.toUpperCase() },
            },
          ],
        },
      ],
    };

    render(<FormBuilder {...props} />);

    userEvent.paste(screen.getByLabelText(/text field/i), 'value');

    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(expect.objectContaining({ values: { field: 'VALUE' } }), expect.anything());
  });

  it('should reset the state if data comes in from an external source (e.g. API Request) and dependencies should be recalculated', () => {
    function FormBuilderWrapper({ onChange }: { onChange: jest.MockedFunction<NonNullable<FormBuilderProps['onChange']>> }) {
      const [textFieldValue, setTextFieldValue] = React.useState('');
      const props: FormBuilderProps = {
        onChange,
        groups: [
          {
            key: 'group',
            type: 'layout-field',
            data: { props: { title: 'Initial Layout Title' } },
            dependencies: [
              {
                key: 'field',
                type: 'text-field',
                cond: () => true,
                // Use a dynamic effect to force a Form Builder onChange onMount
                effects: (data) => (data.value === 'enable dependency' ? { props: { title: 'Dependency Layout Title' } } : {}),
              },
            ],
            fields: [
              { key: 'field', type: 'text-field', data: { props: { label: 'Text Field' }, value: textFieldValue } },
              { key: 'button', type: 'button-field', data: { props: { onClick: () => setTextFieldValue('enable dependency') } } },
            ],
          },
        ],
      };

      return <FormBuilder {...props} />;
    }

    const onChange = jest.fn();

    render(<FormBuilderWrapper onChange={onChange} />);

    expect(screen.getByText(/initial layout title/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/text field/i)).toHaveValue('');
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({ field: expect.objectContaining({ value: '' }) }),
          metaData: expect.objectContaining({ field: expect.objectContaining({ baseValue: '' }) }),
        },
      }),
    );

    // Clicking the button should reset the state
    onChange.mockClear();
    userEvent.click(screen.getByText(/button/i));
    expect(screen.getByText(/dependency layout title/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/text field/i)).toHaveValue('enable dependency');
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({ field: expect.objectContaining({ value: 'enable dependency' }) }),
          metaData: expect.objectContaining({ field: expect.objectContaining({ baseValue: 'enable dependency' }) }),
        },
      }),
    );
  });

  it('should handle the resetKeys correctly', () => {
    const onChange = jest.fn();

    function createProps(value: string, resetConfig: FormBuilderProps['resetConfig']): FormBuilderProps {
      return {
        onChange,
        resetConfig,
        groups: [
          {
            key: 'group',
            type: 'layout-field',
            fields: [
              { key: 'field', type: 'text-field', data: { value } },
              {
                key: 'button',
                type: 'button-field',
                data: {
                  // Use this button to force onChange events
                  render: ({ data: { props }, setData }) => <button onClick={() => setData({})}>{props?.label}</button>,
                },
              },
            ],
          },
        ],
      };
    }

    const { rerender } = render(
      <FormBuilder {...createProps('Initial Value', { resetKeys: { data: [], metaData: [], layout: [] } })}>
        {(formBuilderContent) => <React.Fragment>Initial Layout {formBuilderContent}</React.Fragment>}
      </FormBuilder>,
    );
    userEvent.click(screen.getByText(/button/i));

    expect(screen.getByText(/initial layout/i)).toBeInTheDocument();
    expect(screen.getByDisplayValue(/initial value/i)).toBeInTheDocument();
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({ field: expect.objectContaining({ value: 'Initial Value' }) }),
          metaData: expect.objectContaining({ field: expect.objectContaining({ baseValue: 'Initial Value' }) }),
        },
      }),
    );

    // Rerendering with the same reset keys shouldn't change anything
    onChange.mockClear();
    rerender(
      <FormBuilder {...createProps('Changed Value 1', { resetKeys: { data: [], metaData: [], layout: [] } })}>
        {(formBuilderContent) => <React.Fragment>Changed Layout 1 {formBuilderContent}</React.Fragment>}
      </FormBuilder>,
    );
    userEvent.click(screen.getByText(/button/i));

    expect(screen.getByText(/initial layout/i)).toBeInTheDocument();
    expect(screen.getByDisplayValue(/initial value/i)).toBeInTheDocument();
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({ field: expect.objectContaining({ value: 'Initial Value' }) }),
          metaData: expect.objectContaining({ field: expect.objectContaining({ baseValue: 'Initial Value' }) }),
        },
      }),
    );

    // Rerendering with different data reset keys should only change the data
    onChange.mockClear();
    rerender(
      <FormBuilder {...createProps('Changed Value 2', { resetKeys: { data: [true], metaData: [], layout: [] } })}>
        {(formBuilderContent) => <React.Fragment>Changed Layout 2 {formBuilderContent}</React.Fragment>}
      </FormBuilder>,
    );
    userEvent.click(screen.getByText(/button/i));

    expect(screen.getByText(/initial layout/i)).toBeInTheDocument();
    expect(screen.getByDisplayValue(/changed value 2/i)).toBeInTheDocument();
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({ field: expect.objectContaining({ value: 'Changed Value 2' }) }),
          metaData: expect.objectContaining({ field: expect.objectContaining({ baseValue: 'Initial Value' }) }),
        },
      }),
    );

    // Rerendering with different metaData reset keys should only change the metaData
    onChange.mockClear();
    rerender(
      <FormBuilder {...createProps('Changed Value 3', { resetKeys: { data: [true], metaData: [true], layout: [] } })}>
        {(formBuilderContent) => <React.Fragment>Changed Layout 3 {formBuilderContent}</React.Fragment>}
      </FormBuilder>,
    );
    userEvent.click(screen.getByText(/button/i));

    expect(screen.getByText(/initial layout/i)).toBeInTheDocument();
    expect(screen.getByDisplayValue(/changed value 2/i)).toBeInTheDocument();
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({ field: expect.objectContaining({ value: 'Changed Value 2' }) }),
          metaData: expect.objectContaining({ field: expect.objectContaining({ baseValue: 'Changed Value 3' }) }),
        },
      }),
    );

    // Rerendering with different layout reset keys should only change the layout
    onChange.mockClear();
    rerender(
      <FormBuilder {...createProps('Changed Value 4', { resetKeys: { data: [true], metaData: [true], layout: [true] } })}>
        {(formBuilderContent) => <React.Fragment>Changed Layout 4 {formBuilderContent}</React.Fragment>}
      </FormBuilder>,
    );
    userEvent.click(screen.getByText(/button/i));

    expect(screen.getByText(/changed layout 4/i)).toBeInTheDocument();
    expect(screen.getByDisplayValue(/changed value 2/i)).toBeInTheDocument();
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({ field: expect.objectContaining({ value: 'Changed Value 2' }) }),
          metaData: expect.objectContaining({ field: expect.objectContaining({ baseValue: 'Changed Value 3' }) }),
        },
      }),
    );
  });

  it('should handle the resetStateRef correctly', () => {
    function ResetStateRefWrapper({ value, onChange }: { value: string; onChange: FormBuilderProps['onChange'] }) {
      const [resetData, forceResetData] = React.useState(0);
      const [resetMetaData, forceResetMetaData] = React.useState(0);

      const resetStateRef: ResetStateRef = React.useRef();

      React.useEffect(() => {
        resetStateRef.current?.resetData((nextData) => nextData);
      }, [resetData]);

      React.useEffect(() => {
        resetStateRef.current?.resetMetaData((nextMetaData) => nextMetaData);
      }, [resetMetaData]);

      const props: FormBuilderProps = {
        onChange,
        resetConfig: {
          resetKeys: { data: [], metaData: [], layout: [] },
          resetStateRef,
        },
        groups: [
          {
            key: 'group',
            type: 'layout-field',
            fields: [
              { key: 'field', type: 'text-field', data: { value } },
              {
                key: 'button',
                type: 'button-field',
                data: {
                  // Use this button to force onChange events
                  render: ({ setData }) => <button onClick={() => setData({})}>On Change Button</button>,
                },
              },
              {
                key: 'reset-data-button',
                type: 'button-field',
                data: { props: { label: 'Reset Data', onClick: () => forceResetData((c) => c + 1) } },
              },
              {
                key: 'reset-meta-data-button',
                type: 'button-field',
                data: { props: { label: 'Reset Meta Data', onClick: () => forceResetMetaData((c) => c + 1) } },
              },
              {
                key: 'custom-state-update',
                type: 'button-field',
                data: {
                  props: {
                    label: 'Custom State Update',
                    onClick: () => {
                      resetStateRef.current?.resetData((_nextData, prevData) => ({
                        ...prevData,
                        field: { ...prevData.field, value: 'Custom value update' },
                      }));

                      resetStateRef.current?.resetMetaData((_nextMetaData, prevMetaData) => ({
                        ...prevMetaData,
                        field: { ...prevMetaData.field, baseValue: 'An unrelated base value' },
                      }));
                    },
                  },
                },
              },
            ],
          },
        ],
      };

      return <FormBuilder {...props} />;
    }
    const onChange = jest.fn();

    const { rerender } = render(<ResetStateRefWrapper value="Initial Value" onChange={onChange} />);
    userEvent.click(screen.getByText(/on change button/i));

    expect(screen.getByDisplayValue(/initial value/i)).toBeInTheDocument();
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({ field: expect.objectContaining({ value: 'Initial Value' }) }),
          metaData: expect.objectContaining({ field: expect.objectContaining({ baseValue: 'Initial Value' }) }),
        },
      }),
    );

    // Forcing the data to reset should only affect the data
    onChange.mockClear();
    rerender(<ResetStateRefWrapper value="Changed Value 1" onChange={onChange} />);
    userEvent.click(screen.getByText(/reset data/i));
    userEvent.click(screen.getByText(/on change button/i));

    expect(screen.getByDisplayValue(/changed value 1/i)).toBeInTheDocument();
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({ field: expect.objectContaining({ value: 'Changed Value 1' }) }),
          metaData: expect.objectContaining({ field: expect.objectContaining({ baseValue: 'Initial Value' }) }),
        },
      }),
    );

    // Forcing the metaData to reset should only affect the metaData
    onChange.mockClear();
    rerender(<ResetStateRefWrapper value="Changed Value 2" onChange={onChange} />);
    userEvent.click(screen.getByText(/reset meta data/i));
    userEvent.click(screen.getByText(/on change button/i));

    expect(screen.getByDisplayValue(/changed value 1/i)).toBeInTheDocument();
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({ field: expect.objectContaining({ value: 'Changed Value 1' }) }),
          metaData: expect.objectContaining({ field: expect.objectContaining({ baseValue: 'Changed Value 2' }) }),
        },
      }),
    );

    // Can update the internal state from anywhere via the resetStateRef
    onChange.mockClear();
    userEvent.click(screen.getByText(/custom state update/i));
    userEvent.click(screen.getByText(/on change button/i));

    expect(screen.getByDisplayValue(/custom value update/i)).toBeInTheDocument();
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({ field: expect.objectContaining({ value: 'Custom value update' }) }),
          metaData: expect.objectContaining({ field: expect.objectContaining({ baseValue: 'An unrelated base value' }) }),
        },
      }),
    );
  });

  it('should omit values in the onChange correctly', () => {
    const onChange = jest.fn();

    const props: FormBuilderProps = {
      onChange,
      groups: [
        {
          key: 'group',
          type: 'layout-field',
          fields: [
            { key: 'field', type: 'text-field', data: { props: { label: 'Text Field To Change' } } },
            {
              key: 'field2',
              type: 'text-field',
              data: { props: { label: 'Field to Omit' }, value: 'This value will be omitted' },
              metaData: { omit: true },
            },
          ],
        },
        {
          key: 'hidden-group',
          type: 'layout-field',
          data: { isHidden: true },
          metaData: { omitFieldsOnHide: true },
          fields: [
            { key: 'field3', type: 'text-field', data: { value: 'This value will be omitted because the group is hidden' } },
            { key: 'field4', type: 'text-field', data: { value: 'Field 4' }, metaData: { omit: false } },
            { key: 'field5', type: 'text-field', data: { value: 'Field 5' }, metaData: { omit: () => false } },
          ],
        },
      ],
    };

    render(<FormBuilder {...props} />);

    userEvent.paste(screen.getByLabelText(/text field to change/i), 'modified value');
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.objectContaining({ values: { field: 'modified value', field4: 'Field 4', field5: 'Field 5' } }),
      expect.anything(),
    );
  });

  it('should handle state updaters correctly', () => {
    const onChange = jest.fn();

    const props: FormBuilderProps = {
      onChange,
      groups: [
        {
          key: 'group',
          type: 'layout-field',
          // Use a dependency to force the onChange event
          dependencies: [{ key: 'group', type: 'layout-field', cond: () => true, effects: {} }],
          fields: [
            {
              key: 'field1',
              type: 'text-field',
              data: {
                props: { label: 'Field 1' },
                render: ({ data: { props, value }, setData, setMetaData, dispatch }) => (
                  <label>
                    {props?.label}
                    <input
                      value={value}
                      onChange={(event) => {
                        setData({ value: event.target.value });
                        setMetaData({ groupKey: 'Form Key Changed From Field 1' });

                        // Update field two from the field one onChange (the meta data update needs to happen first because that doesn't trigger an onChange like a date update will)
                        setDependentKeyMetaData(dispatch, 'field2', { groupKey: 'Form Key Changed From Field 1' });
                        setDependentKeyData(dispatch, 'field2', { value: event.target.value });
                      }}
                    />
                  </label>
                ),
              },
            },
            {
              key: 'field2',
              type: 'text-field',
              data: {
                props: { label: 'Field 2' },
              },
            },
          ],
        },
      ],
    };

    render(<FormBuilder {...props} />);
    const fieldOne = screen.getByLabelText(/field 1/i);
    const fieldTwo = screen.getByLabelText(/field 2/i);

    expect(fieldOne).toHaveValue('');
    expect(fieldTwo).toHaveValue('');
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({ field1: expect.objectContaining({ value: '' }), field2: expect.objectContaining({ value: '' }) }),
          metaData: expect.objectContaining({
            field1: expect.objectContaining({ groupKey: 'group' }),
            field2: expect.objectContaining({ groupKey: 'group' }),
          }),
        },
      }),
    );

    // Changing field one should update field two
    onChange.mockClear();
    userEvent.paste(fieldOne, 'changed value');

    expect(fieldOne).toHaveValue('changed value');
    expect(fieldTwo).toHaveValue('changed value');
    expect(onChange).toHaveBeenCalledTimes(2);
    expect(onChange).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        current: {
          data: expect.objectContaining({
            field1: expect.objectContaining({ value: 'changed value' }),
            field2: expect.objectContaining({ value: 'changed value' }),
          }),
          metaData: expect.objectContaining({
            field1: expect.objectContaining({ groupKey: 'Form Key Changed From Field 1' }),
            field2: expect.objectContaining({ groupKey: 'Form Key Changed From Field 1' }),
          }),
        },
      }),
    );
  });

  it.todo('should handle plugins at a form level correctly');
});

/* Utilities */
type FormFields = {
  ['text-field']: { type: 'text-field'; props: { label: string }; value: string };
  ['button-field']: { type: 'button-field'; props: { label?: string; onClick?: () => void }; value: never };
};

type LayoutFields = {
  ['layout-field']: { type: 'layout-field'; props: { title: string } };
};

function generateFormBuiderConfig() {
  const config: FormBuilderConfig<FormFields, LayoutFields> = {
    defaultFormFieldConfigs: {
      'button-field': {
        data: {
          props: { label: 'button' },
          render: ({ data: { props } }) => <button onClick={props?.onClick}>{props?.label}</button>,
        },
      },
      'text-field': {
        data: {
          props: { label: 'Default Text Field' },
          render: ({ data: { props, value }, setData }) => (
            <label>
              {props?.label}
              <input value={value} onChange={(event) => setData({ value: event.target.value })} />
            </label>
          ),
          value: '',
        },
      },
    },
    defaultLayoutFieldConfigs: {
      'layout-field': {
        data: {
          props: { title: 'Default Layout Title' },
          render: ({ data: { props }, fields }) => (
            <div>
              {props?.title} {fields}
            </div>
          ),
        },
      },
    },
  };

  return configureFormBuilder<FormFields, LayoutFields>(config);
}
