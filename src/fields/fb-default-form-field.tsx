import { FBFormFieldDependencyType } from './fb-field.type';
import { useFormBuilderFieldState } from '../hooks/use-form-builder-field-state';

function FBDefaultFormField({ fieldKey, baseData = {}, baseMetaData = {} }: FBDefaultFormFieldProps) {
  const {
    data: { render, ...data },
    ...rest
  } = useFormBuilderFieldState<Data, MetaData>(fieldKey, baseData, baseMetaData);

  /* istanbul ignore next */
  if (process.env.NODE_ENV !== 'production') {
    if (!render) {
      const error = new Error(`A render function needs to be provided for the field with the key \`${fieldKey}\`.`);
      throw error;
    }
  }

  return render && !data.isHidden ? render({ data, ...rest }) : null;
}

/* Utilities */
type Data = FBFormFieldDependencyType[string]['data'];
type MetaData = FBFormFieldDependencyType[string]['metaData'];
type FBDefaultFormFieldProps = { fieldKey: string; baseData: Data; baseMetaData: MetaData };

export { FBDefaultFormField };
