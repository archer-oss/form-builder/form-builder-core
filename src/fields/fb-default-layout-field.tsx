import { FBLayoutFieldDependencyType, FieldKey } from './fb-field.type';
import { useFormBuilderFieldState } from '../hooks/use-form-builder-field-state';

function FBDefaultLayoutField({ fieldKey, baseData, baseMetaData, fields, fieldMap }: FBDefaultLayoutFieldProps) {
  const {
    data: { render, ...data },
    ...rest
  } = useFormBuilderFieldState<Data, MetaData>(fieldKey, baseData, baseMetaData);

  /* istanbul ignore next */
  if (process.env.NODE_ENV !== 'production') {
    if (!render) {
      const error = new Error(`A render function needs to be provided for the field with the key \`${fieldKey}\`.`);
      throw error;
    }
  }

  return render && !data.isHidden ? render({ fields, fieldMap, data, ...rest }) : null;
}

/* Utilities */
type Data = FBLayoutFieldDependencyType[string]['data'];
type MetaData = FBLayoutFieldDependencyType[string]['metaData'];
type FBDefaultLayoutFieldProps = {
  fieldKey: string;
  baseData: Data;
  baseMetaData: MetaData;
  fields: JSX.Element[];
  fieldMap: Record<FieldKey, JSX.Element>;
};

export { FBDefaultLayoutField };
