import {
  DependencyCheckerProviderDispatch,
  BaseData,
  BaseMetaData,
  BaseDependencies,
  DataCollection,
  MetaDataCollection,
  StateUpdater,
} from '@archer-oss/dependency-checker-react';

/**
 * Form Field Dependency Types
 * Format: { type: string; data: Record<string, any>; metaData: Record<string, any> }
 * The key used for the field should match the type of that field.
 */
export type FBFormFieldConfig = { type: string; props: Record<string, any>; value: any };
export type FBFormFieldConfigCollection = Record<string, FBFormFieldConfig>;

export type CreateFBFormFieldDependencyType<
  FieldConfig extends FBFormFieldConfig,
  FieldData = {
    props?: FieldConfig['props'];
    value?: FieldConfig['value'];
    isHidden?: boolean;
    isValid?: boolean;
    isDirty?: boolean;
  },
  FieldMetaData = BaseFormFieldMetaData<FieldConfig['type'], FieldData>,
> = {
  type: FieldConfig['type'];
  data: FieldData & { render?: (config: FormFieldRenderConfig<FieldData, FieldMetaData>) => JSX.Element };
  metaData: FieldMetaData;
};

export type FBFormFieldDependencyType<Configs extends FBFormFieldConfigCollection = FBFormFieldConfigCollection> = {
  [Type in keyof Configs]: CreateFBFormFieldDependencyType<Configs[Type]>;
};

/**
 * Layout Field Dependency Types
 * Format: { type: string; data: Record<string, any>; metaData: Record<string, any> }
 * The key used for the field should match the type of that field.
 */
export type FBLayoutFieldConfig = { type: string; props: Record<string, any> };
export type FBLayoutFieldConfigCollection = Record<string, FBLayoutFieldConfig>;

export type CreateFBLayoutFieldDependencyType<
  FieldConfig extends FBLayoutFieldConfig,
  FieldData = {
    props?: FieldConfig['props'];
    isHidden?: boolean;
  },
  FieldMetaData = BaseLayoutFieldMetaData<FieldConfig['type']>,
> = {
  type: FieldConfig['type'];
  data: FieldData & {
    render?: (config: LayoutFieldRenderConfig<FieldData, FieldMetaData>) => JSX.Element;
  };
  metaData: FieldMetaData;
};

export type FBLayoutFieldDependencyType<Configs extends FBLayoutFieldConfigCollection = FBLayoutFieldConfigCollection> = {
  [Type in keyof Configs]: CreateFBLayoutFieldDependencyType<Configs[Type]>;
};

/**
 * Utilities
 */
export type FieldKey = string;

type DefaultFieldConfig = Record<string, { data?: BaseData; metaData?: BaseMetaData }>;

export type DefaultFieldConfigs<Configs extends DefaultFieldConfig> = {
  [Key in keyof Configs]?: {
    data?: Configs[Key]['data'];
    metaData?: Configs[Key]['metaData'];
    dependencies?: BaseDependencies;
  };
};

export type BaseFormFieldMetaData<Type extends string, Data extends BaseData> = {
  baseValue?: Data['value'];
  fieldKey?: string;
  fieldType?: Type;
  formKey?: string;
  groupKey?: string;
  info?: Record<string, any>;
  normalizedFieldKey?: string;
  omit?: ((data: DataCollection, metaData: MetaDataCollection) => boolean) | boolean;
  touched?: boolean;
  valueFormatter?: (value: Data['value'], data: DataCollection, metaData: MetaDataCollection) => any;
};

export type BaseLayoutFieldMetaData<Type extends string> = {
  fieldKey?: string;
  fieldType?: Type;
  fieldWrapperRender?: (fieldContent: JSX.Element, config: FormFieldRenderConfig<BaseData, BaseMetaData>) => JSX.Element;
  formKey?: string;
  info?: Record<string, any>;
  normalizedFieldKey?: string;
  omit?: ((data: DataCollection, metaData: MetaDataCollection) => boolean) | boolean;
  omitFieldsOnHide?: boolean;
};

export type FormFieldRenderConfig<Data extends BaseData, MetaData extends BaseMetaData> = {
  data: Data;
  metaData: MetaData;
  setData: (fieldData: StateUpdater<Data>) => void;
  setMetaData: (fieldMetaData: StateUpdater<MetaData>) => void;
  dispatch: DependencyCheckerProviderDispatch;
};

export type LayoutFieldRenderConfig<Data extends BaseData, MetaData extends BaseMetaData> = {
  data: Data;
  metaData: MetaData;
  setData: (fieldData: StateUpdater<Data>) => void;
  setMetaData: (fieldMetaData: StateUpdater<MetaData>) => void;
  dispatch: DependencyCheckerProviderDispatch;
  fields: JSX.Element[];
  fieldMap: Record<FieldKey, JSX.Element>;
};
