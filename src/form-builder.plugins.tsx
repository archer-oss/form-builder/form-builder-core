import { BaseData, BaseMetaData, BaseDependencies, merge } from '@archer-oss/dependency-checker-react';
import { FBFormFieldConfig, FBFormFieldConfigCollection, FBLayoutFieldConfigCollection, FBLayoutFieldConfig } from './fields/fb-field.type';
import { FormBuilderField, FormBuilderGroup } from './layout/form-builder-layout.component';
import { FBManagerBaseOptions } from './hooks/use-form-builder-manager';

/* Build Field Plugins */
const addBaseValueToField: BuildField = (field) => ({
  ...field,
  metaData: field.data?.value !== undefined ? { baseValue: field.data.value, ...field.metaData } : { ...field.metaData },
});
const addFieldAndFormAndGroupKeysToField: BuildField = (field, { key: groupKey }, { formKey }) => ({
  ...field,
  metaData: { fieldKey: field.key, formKey, groupKey, ...field.metaData },
});
const mergeDefaultFieldConfiguration: BuildField = (field, _group, options) => mergeDefaultConfiguration(field, options);
const addFieldWrapper: BuildField = (field, group) => {
  if (!group.metaData?.fieldWrapperRender || !field.data?.render) return field;
  const { fieldWrapperRender } = group.metaData;
  const { render } = field.data;

  return { ...field, data: { ...field.data, render: (config) => fieldWrapperRender(render(config), config) } };
};

/* Build Group Plugins */
const addFieldAndFormKeyToGroup: BuildGroup = (group, options) => ({
  ...group,
  metaData: { fieldKey: group.key, formKey: options.formKey, ...group.metaData },
});
const omitGroupByDefault: BuildGroup = (group) => ({ ...group, metaData: { omit: true, ...group.metaData } });
const mergeDefaultGroupConfiguration: BuildGroup = (group, options) => mergeDefaultConfiguration(group, options);

export const defaultPlugins: FBPlugins = {
  buildField: [addBaseValueToField, addFieldAndFormAndGroupKeysToField, mergeDefaultFieldConfiguration, addFieldWrapper],
  buildGroup: [addFieldAndFormKeyToGroup, omitGroupByDefault, mergeDefaultGroupConfiguration],
};

/* Utilities */
export type BuildField<
  FormFields extends Record<string, FBFormFieldConfig> = Record<string, FBFormFieldConfig>,
  LayoutFields extends Record<string, FBLayoutFieldConfig> = Record<string, FBLayoutFieldConfig>,
> = (
  field: FormBuilderField<FormFields, LayoutFields>,
  group: FormBuilderGroup<FormFields, LayoutFields>,
  options: Omit<FBManagerBaseOptions<FormFields, LayoutFields>, 'plugins'>,
) => FormBuilderField<FormFields, LayoutFields>;

export type BuildGroup<
  FormFields extends Record<string, FBFormFieldConfig> = Record<string, FBFormFieldConfig>,
  LayoutFields extends Record<string, FBLayoutFieldConfig> = Record<string, FBLayoutFieldConfig>,
> = (
  group: FormBuilderGroup<FormFields, LayoutFields>,
  options: Omit<FBManagerBaseOptions<FormFields, LayoutFields>, 'plugins'>,
) => FormBuilderGroup<FormFields, LayoutFields>;

export type FBPlugins<
  FormFields extends Record<string, FBFormFieldConfig> = Record<string, FBFormFieldConfig>,
  LayoutFields extends Record<string, FBLayoutFieldConfig> = Record<string, FBLayoutFieldConfig>,
> = {
  buildField: BuildField<FormFields, LayoutFields>[];
  buildGroup: BuildGroup<FormFields, LayoutFields>[];
};

type DefaultFBManagerOptions = Omit<FBManagerBaseOptions<FBFormFieldConfigCollection, FBLayoutFieldConfigCollection>, 'plugins'>;

function mergeDefaultConfiguration<T extends { type: string; data?: BaseData; metaData?: BaseMetaData; dependencies?: BaseDependencies }>(
  { type, data = {}, metaData = {}, dependencies = [], ...rest }: T,
  { defaultFieldConfigs }: DefaultFBManagerOptions,
) {
  return {
    ...rest,
    type,
    data: merge({}, defaultFieldConfigs[type]?.data ?? {}, data),
    metaData: merge({}, defaultFieldConfigs[type]?.metaData ?? {}, metaData),
    dependencies: [...(defaultFieldConfigs[type]?.dependencies ?? []), ...dependencies],
  };
}
