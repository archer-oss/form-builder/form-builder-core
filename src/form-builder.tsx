import * as React from 'react';
import {
  BaseData,
  BaseMetaData,
  DependencyCheckerProvider,
  DependencyCheckerProviderProps,
  ResetStateRef,
  changedArray,
} from '@archer-oss/dependency-checker-react';
import { FBFormFieldConfigCollection, FBLayoutFieldConfigCollection } from './fields/fb-field.type';
import { FormBuilderLayout } from './layout/form-builder-layout.component';
import { useBaseFormBuilderManager, FBManagerConfig, FBManagerOptions } from './hooks/use-form-builder-manager';

/**
 * Multi Form Builder
 */
export function MultiFormBuilder(props: MultiFormBuilderProps) {
  const { onChange, dependencyCheckerRate, resetConfig, formConfigurations, children } = props;

  let data: DependencyCheckerProviderProps['data'] = {};
  let metaData: DependencyCheckerProviderProps['metaData'] = {};
  let dependencies: NonNullable<DependencyCheckerProviderProps['dependencies']> = {};

  formConfigurations.forEach((config) => {
    /* istanbul ignore next */
    if (process.env.NODE_ENV !== 'production') {
      handleDuplicateKeyError(data, config.data);
      handleDuplicateKeyError(metaData, config.metaData);
      handleDuplicateKeyError(dependencies, config.dependencies);
    }

    data = { ...data, ...config.data };
    metaData = { ...metaData, ...config.metaData };
    dependencies = { ...dependencies, ...config.dependencies };
  });

  return (
    <DependencyCheckerProvider
      data={data}
      metaData={metaData}
      dependencies={dependencies}
      resetConfig={resetConfig}
      dependencyCheckerRate={dependencyCheckerRate}
      onChange={(onChangeData, onChangeMetaData, details) =>
        onChange &&
        onChange(normalizeFormData(onChangeData, onChangeMetaData), { ...details, current: { data: onChangeData, metaData: onChangeMetaData } })
      }
    >
      <FormBuilderLayoutWrapper resetKeys={resetConfig?.resetKeys?.layout}>{children}</FormBuilderLayoutWrapper>
    </DependencyCheckerProvider>
  );
}

/**
 * Single Form Builder
 */
export function BaseFormBuilder({
  useFormBuilderManager,
  ...props
}: BaseFormBuilderProps<FBFormFieldConfigCollection, FBLayoutFieldConfigCollection>) {
  const { onChange, dependencyCheckerRate, resetConfig, children, fbManagerOptions = {}, ...fbManagerConfig } = props;
  const formConfiguration = useFormBuilderManager(fbManagerConfig, fbManagerOptions);
  const formBuilderContent = <FormBuilderLayout layoutConfig={formConfiguration.layoutConfig} />;

  return (
    <MultiFormBuilder
      dependencyCheckerRate={dependencyCheckerRate}
      formConfigurations={[formConfiguration]}
      onChange={onChange}
      resetConfig={resetConfig}
    >
      {children ? children(formBuilderContent) : formBuilderContent}
    </MultiFormBuilder>
  );
}

/**
 * Utilities
 */
export type MultiFormBuilderProps = {
  children: React.ReactNode;
  dependencyCheckerRate?: number;
  formConfigurations: ReturnType<typeof useBaseFormBuilderManager>[];
  onChange?: NormalizedFormDataHandler;
  resetConfig?: {
    resetKeys?: Partial<Record<'data' | 'metaData' | 'layout', any[]>>;
    resetStateRef?: ResetStateRef;
  };
};

export type BaseFormBuilderProps<FormFields extends FBFormFieldConfigCollection, LayoutFields extends FBLayoutFieldConfigCollection> = Pick<
  MultiFormBuilderProps,
  'dependencyCheckerRate' | 'onChange' | 'resetConfig'
> &
  FBManagerConfig<FormFields, LayoutFields> & {
    children?: (formBuilderContent: JSX.Element) => JSX.Element;
    fbManagerOptions?: FBManagerOptions<FormFields, LayoutFields>;
    useFormBuilderManager: (
      config: FBManagerConfig<FormFields, LayoutFields>,
      options: FBManagerOptions<FormFields, LayoutFields>,
    ) => ReturnType<typeof useBaseFormBuilderManager>;
  };

export type FormBuilderProps<FormFields extends FBFormFieldConfigCollection, LayoutFields extends FBLayoutFieldConfigCollection> = Omit<
  BaseFormBuilderProps<FormFields, LayoutFields>,
  'useFormBuilderManager'
>;

export type NormalizedData<Values extends Record<string, any> = Record<string, any>> = {
  values: Values;
  isValid: boolean;
  isDirty: boolean;
  isTouched: boolean;
};

type OnChangeDetails = Parameters<NonNullable<DependencyCheckerProviderProps['onChange']>>[2];
export type NormalizedFormDataHandler = (
  normalizedData: NormalizedData,
  details: OnChangeDetails & {
    // TODO: Add the previous data and metaData
    current: {
      data: Record<string, BaseData | undefined>;
      metaData: Record<string, BaseMetaData>;
    };
  },
) => void;

function normalizeFormData(data: Record<string, BaseData | undefined>, metaData: Record<string, BaseMetaData>) {
  // TODO: Add form level state by formKey
  const normalizedData: NormalizedData = { values: {}, isValid: true, isDirty: false, isTouched: false };
  const filteredFields = filterOmittedAndHiddenFields(data, metaData);
  const filteredFieldKeys = Object.keys(filteredFields.data);

  filteredFieldKeys.forEach((key) => {
    const { isValid = true, isDirty = false, props = {}, value } = data[key] || {};
    const { checkValidityWhenDisabled = false, formKey, normalizedFieldKey = key, touched = false, valueFormatter } = metaData[key] || {};

    if (formKey) {
      if (!normalizedData.values[formKey]) {
        normalizedData.values[formKey] = {};
      }
      normalizedData.values[formKey][normalizedFieldKey] = valueFormatter ? valueFormatter(value, data, metaData) : value;
    } else {
      normalizedData.values[normalizedFieldKey] = valueFormatter ? valueFormatter(value, data, metaData) : value;
    }

    if (checkValidityWhenDisabled) {
      normalizedData.isValid = normalizedData.isValid && isValid;
    } else {
      normalizedData.isValid = normalizedData.isValid && (!!props.disabled || isValid);
    }

    normalizedData.isDirty = normalizedData.isDirty || isDirty;
    normalizedData.isTouched = normalizedData.isTouched || touched;
  });

  return normalizedData;
}

const FormBuilderLayoutWrapper = React.memo(
  ({ children }: { resetKeys?: any[]; children: React.ReactNode }) => <>{children}</>,
  (prevProps, nextProps) => !changedArray(prevProps.resetKeys, nextProps.resetKeys),
);

/* Re-export Form Builder utils */
// export * from './form-builder.utils';

export function filterOmittedAndHiddenFields(data: Record<string, BaseData | undefined>, metaData: Record<string, BaseMetaData>) {
  return filterFormDataBy(data, metaData, (fieldData, fieldMetaData) => {
    const groupData = data[fieldMetaData.groupKey] || {};
    const groupMetaData = metaData[fieldMetaData.groupKey] || {};

    // If omit is explicitly set at the field level then always respect it regardless of other conditions (e.g. the field is hidden)
    const omit = typeof fieldMetaData.omit === 'function' ? fieldMetaData.omit(data, metaData) : fieldMetaData.omit;
    if (typeof omit === 'boolean') {
      return !omit;
    }

    // Omit a field if its group is hidden and omitFieldsOnHide is enabled at the group level
    if (!!groupMetaData.omitFieldsOnHide && !!groupData.isHidden) {
      return false;
    }

    // Keep a field if it is not hidden
    return !fieldData.isHidden;
  });
}

export type FilterFormDataByData = Record<string, BaseData | undefined>;
export type FilterFormDataByMetaData = Record<string, BaseMetaData>;
export type FilterFormDataByFn = (data: BaseData, metaData: BaseMetaData) => boolean;

export function filterFormDataBy(data: FilterFormDataByData, metaData: FilterFormDataByMetaData, filterFn: FilterFormDataByFn) {
  const filteredData: Record<string, BaseData | undefined> = {};
  const filteredMetaData: Record<string, BaseMetaData> = {};

  Object.keys(data).forEach((key) => {
    if (filterFn(data[key] ?? {}, metaData[key] ?? {})) {
      filteredData[key] = data[key];
      filteredMetaData[key] = metaData[key];
    }
  });

  return { data: filteredData, metaData: filteredMetaData };
}

/* istanbul ignore next */
function handleDuplicateKeyError(existingConfigInfo: Record<string, any>, newConfigInfo: Record<string, any>) {
  const existingKeys = Object.keys(existingConfigInfo);
  const newKeys = Object.keys(newConfigInfo);

  for (const key of newKeys) {
    if (existingKeys.includes(key)) {
      const error = new Error(`The form builder key \`${key}\` has already been used. Make sure to use unique keys.`);
      Error.captureStackTrace && Error.captureStackTrace(error, handleDuplicateKeyError);
      throw error;
    }
  }
}
