import * as React from 'react';
import {
  useDependencyCheckerDispatch,
  subscribeToDependentKeyChanges,
  setDependentKeyData,
  setDependentKeyMetaData,
  BaseData,
  BaseMetaData,
  StateUpdater,
} from '@archer-oss/dependency-checker-react';

export function useFormBuilderFieldState<FieldData extends BaseData = BaseData, FieldMetaData extends BaseMetaData = BaseMetaData>(
  fieldKey: string,
  baseData: BaseData = {},
  baseMetaData: BaseMetaData = {},
) {
  const dispatch = useDependencyCheckerDispatch();
  const [{ data, metaData }, setState] = React.useState({ data: baseData, metaData: baseMetaData });

  React.useEffect(() => {
    const unsubscribe = subscribeToDependentKeyChanges(dispatch, fieldKey, (fieldData, fieldMetaData) => {
      setState({ data: fieldData, metaData: fieldMetaData });
    });
    return unsubscribe;
  }, [fieldKey, dispatch]);

  const setData = React.useCallback(
    (fieldDataUpdater: StateUpdater<BaseData>) => {
      setDependentKeyData(dispatch, fieldKey, fieldDataUpdater);
    },
    [fieldKey, dispatch],
  );

  const setMetaData = React.useCallback(
    (fieldMetaDataUpdater: StateUpdater<BaseMetaData>) => {
      setDependentKeyMetaData(dispatch, fieldKey, fieldMetaDataUpdater);
    },
    [fieldKey, dispatch],
  );

  return {
    data: data as FieldData,
    setData: setData as (fieldData: StateUpdater<Partial<FieldData>>) => void,
    metaData: metaData as FieldMetaData,
    setMetaData: setMetaData as (fieldMetaData: StateUpdater<Partial<FieldMetaData>>) => void,
    dispatch,
  };
}
