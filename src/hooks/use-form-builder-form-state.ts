import * as React from 'react';
import {
  useDependencyCheckerDispatch,
  subscribeToStateChanges,
  setDependentKeyData,
  setDependentKeyMetaData,
  BaseData,
  BaseMetaData,
  StateUpdater,
} from '@archer-oss/dependency-checker-react';

export function useFormBuilderFormState() {
  const dispatch = useDependencyCheckerDispatch();
  const [{ data, metaData }, setState] = React.useState<FormState>({ data: {}, metaData: {} });

  React.useEffect(() => {
    const unsubscribe = subscribeToStateChanges(dispatch, (formData, formMetaData) => {
      setState({ data: formData, metaData: formMetaData });
    });
    return () => unsubscribe();
  }, [dispatch]);

  const setData = React.useCallback(
    (fieldKey: string, fieldDataUpdater: StateUpdater<BaseData>) => {
      setDependentKeyData(dispatch, fieldKey, fieldDataUpdater);
    },
    [dispatch],
  );

  const setMetaData = React.useCallback(
    (fieldKey: string, fieldMetaDataUpdater: StateUpdater<BaseMetaData>) => {
      setDependentKeyMetaData(dispatch, fieldKey, fieldMetaDataUpdater);
    },
    [dispatch],
  );

  return {
    data: data as FormState['data'],
    setData: setData as <Data extends BaseData = BaseData>(fieldKey: string, fieldData: StateUpdater<Partial<Data>>) => void,
    metaData: metaData as FormState['metaData'],
    setMetaData: setMetaData as <MetaData extends BaseMetaData = BaseMetaData>(
      fieldKey: string,
      fieldMetaData: StateUpdater<Partial<MetaData>>,
    ) => void,
  };
}

export type FormStateConfig = ReturnType<typeof useFormBuilderFormState>;

/**
 * Utilities
 */
type FormState = { data: BaseData; metaData: BaseMetaData };
