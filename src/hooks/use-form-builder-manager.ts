import { DataCollection, MetaDataCollection, DependenciesCollection } from '@archer-oss/dependency-checker-react';
import {
  DefaultFieldConfigs,
  FBFormFieldConfigCollection,
  FBFormFieldDependencyType,
  FBLayoutFieldConfigCollection,
  FBLayoutFieldDependencyType,
} from '../fields/fb-field.type';
import { FormBuilderLayoutConfig } from '../layout/form-builder-layout.component';
import { FBPlugins } from '../form-builder.plugins';

export function useBaseFormBuilderManager<FormFields extends FBFormFieldConfigCollection, LayoutFields extends FBLayoutFieldConfigCollection>(
  fbManagerConfig: FBManagerConfig<FormFields, LayoutFields>,
  { plugins, ...fbManagerOptions }: FBManagerBaseOptions<FormFields, LayoutFields>,
) {
  const data: DataCollection = {};
  const metaData: MetaDataCollection = {};
  const dependencies: DependenciesCollection = {};

  // Separate the layout configuation from the field data, metaData and dependencies
  const layoutConfig: FormBuilderLayoutConfig<FormFields, LayoutFields> = {
    ...fbManagerConfig,
    groups: fbManagerConfig.groups.map((group) => {
      /* istanbul ignore next */
      if (process.env.NODE_ENV !== 'production') {
        data[group.key] !== undefined && handleDuplicateKeyError(group.key);
      }

      const updatedGroup = plugins.buildGroup.reduce((currentGroup, updater) => updater(currentGroup, fbManagerOptions), group);
      data[updatedGroup.key] = updatedGroup.data ?? {};
      metaData[updatedGroup.key] = updatedGroup.metaData ?? {};
      dependencies[updatedGroup.key] = updatedGroup.dependencies ?? [];

      return {
        ...updatedGroup,
        fields: updatedGroup.fields.map((field) => {
          /* istanbul ignore next */
          if (process.env.NODE_ENV !== 'production') {
            data[field.key] !== undefined && handleDuplicateKeyError(field.key);
          }

          const updatedField = plugins.buildField.reduce((currentField, updater) => updater(currentField, updatedGroup, fbManagerOptions), field);
          data[updatedField.key] = updatedField.data ?? {};
          metaData[updatedField.key] = updatedField.metaData ?? {};
          dependencies[updatedField.key] = updatedField.dependencies ?? [];

          return updatedField;
        }),
      };
    }),
  };

  return { layoutConfig, data, metaData, dependencies };
}

/**
 * Utilities
 */
export type FBManagerConfig<
  FormFields extends FBFormFieldConfigCollection,
  LayoutFields extends FBLayoutFieldConfigCollection,
> = FormBuilderLayoutConfig<FormFields, LayoutFields>;

export type FBManagerBaseOptions<FormFields extends FBFormFieldConfigCollection, LayoutFields extends FBLayoutFieldConfigCollection> = {
  defaultFieldConfigs: DefaultFieldConfigs<FBFormFieldDependencyType & FBLayoutFieldDependencyType>;
  formKey?: string;
  plugins: FBPlugins<FormFields, LayoutFields>;
};

export type FBManagerOptions<FormFields extends FBFormFieldConfigCollection, LayoutFields extends FBLayoutFieldConfigCollection> = Omit<
  FBManagerBaseOptions<FormFields, LayoutFields>,
  'defaultFieldConfigs' | 'plugins'
> & {
  plugins?: Partial<FBPlugins<FormFields, LayoutFields>>;
};

/* istanbul ignore next */
function handleDuplicateKeyError(key: string) {
  const error = new Error(`The form builder key \`${key}\` has already been used. Make sure to use unique keys.`);
  Error.captureStackTrace && Error.captureStackTrace(error, useBaseFormBuilderManager);
  throw error;
}
