import * as React from 'react';
import { BaseFormBuilder, MultiFormBuilder, FormBuilderProps } from './form-builder';
import {
  DefaultFieldConfigs,
  FBFormFieldConfigCollection,
  FBFormFieldDependencyType,
  FBLayoutFieldConfigCollection,
  FBLayoutFieldDependencyType,
} from './fields/fb-field.type';
import { useFormBuilderFieldState } from './hooks/use-form-builder-field-state';
import { useFormBuilderFormState } from './hooks/use-form-builder-form-state';
import { useBaseFormBuilderManager, FBManagerConfig, FBManagerOptions } from './hooks/use-form-builder-manager';
import { defaultPlugins, FBPlugins } from './form-builder.plugins';

export type FormBuilderConfig<FormFields extends FBFormFieldConfigCollection, LayoutFields extends FBLayoutFieldConfigCollection> = {
  defaultFormFieldConfigs: DefaultFieldConfigs<FBFormFieldDependencyType<FormFields>>;
  defaultLayoutFieldConfigs: DefaultFieldConfigs<FBLayoutFieldDependencyType<LayoutFields>>;
  plugins?: Partial<FBPlugins<FormFields, LayoutFields>>;
};

function configureFormBuilder<FormFields extends FBFormFieldConfigCollection, LayoutFields extends FBLayoutFieldConfigCollection>({
  defaultFormFieldConfigs = {},
  defaultLayoutFieldConfigs = {},
  plugins: { buildField = [], buildGroup = [] } = {},
}: FormBuilderConfig<FormFields, LayoutFields>) {
  const defaultFieldConfigs = { ...defaultFormFieldConfigs, ...defaultLayoutFieldConfigs };

  const useFormBuilderManager = (
    fbManagerConfig: FBManagerConfig<FormFields, LayoutFields>,
    { plugins, ...fbManagerOptions }: FBManagerOptions<FormFields, LayoutFields> = {},
  ) =>
    useBaseFormBuilderManager(fbManagerConfig, {
      ...fbManagerOptions,
      plugins: {
        buildField: [...defaultPlugins.buildField, ...buildField, ...(plugins?.buildField ?? [])],
        buildGroup: [...defaultPlugins.buildGroup, ...buildGroup, ...(plugins?.buildGroup ?? [])],
      },
      defaultFieldConfigs,
    });

  return {
    FormBuilder: (props: FormBuilderProps<FormFields, LayoutFields>) => {
      return <BaseFormBuilder {...props} useFormBuilderManager={useFormBuilderManager} />;
    },
    MultiFormBuilder,
    useFormBuilderManager,
  };
}

/* Re-export Core Types */
export type { CreateDependenciesTypes } from '@archer-oss/dependency-checker-react';

/* Export Form Builder Types */
export type {
  FBFormFieldConfig,
  FBFormFieldConfigCollection,
  FBFormFieldDependencyType,
  CreateFBFormFieldDependencyType,
  FBLayoutFieldConfig,
  FBLayoutFieldConfigCollection,
  FBLayoutFieldDependencyType,
  CreateFBLayoutFieldDependencyType,
  BaseFormFieldMetaData,
  BaseLayoutFieldMetaData,
  FormFieldRenderConfig,
  LayoutFieldRenderConfig,
} from './fields/fb-field.type';

export type { BuildField, BuildGroup, FBPlugins } from './form-builder.plugins';

/* Re-export Core Logic */
export {
  DependencyCheckerProvider,
  useDependencyCheckerDispatch,
  subscribeToStateChanges,
  subscribeToDependentKeyChanges,
  setDependentKeyData,
  setDependentKeyMetaData,
  generateSnapshot,
  merge,
  uniq,
} from '@archer-oss/dependency-checker-react';

export { configureFormBuilder, useFormBuilderFormState, useFormBuilderFieldState };
