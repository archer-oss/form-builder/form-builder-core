import * as React from 'react';
import { useDependencyCheckerDispatch, generateSnapshot, CreateDependenciesTypes } from '@archer-oss/dependency-checker-react';
import {
  FBFormFieldConfigCollection,
  FBFormFieldDependencyType,
  FBLayoutFieldConfigCollection,
  FBLayoutFieldDependencyType,
} from '../fields/fb-field.type';
import { FBDefaultFormField } from '../fields/fb-default-form-field';
import { FBDefaultLayoutField } from '../fields/fb-default-layout-field';

function FormBuilderLayout({ layoutConfig }: FormBuilderLayoutProps<FBFormFieldConfigCollection, FBLayoutFieldConfigCollection>) {
  const dispatch = useDependencyCheckerDispatch();
  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        generateSnapshot(dispatch);
      }}
    >
      {layoutConfig.groups.map(({ key: groupKey, data: groupData = {}, metaData: groupMetaData = {}, fields }) => {
        const fieldMap: Record<string, JSX.Element> = {};
        const fieldList: JSX.Element[] = [];

        for (const field of fields) {
          const { key: fieldKey, data: fieldData = {}, metaData: fieldMetaData = {} } = field;
          const fieldComponent = <FBDefaultFormField key={fieldKey} fieldKey={fieldKey} baseData={fieldData} baseMetaData={fieldMetaData} />;
          fieldMap[fieldKey] = fieldComponent;
          fieldList.push(fieldComponent);
        }

        return (
          <FBDefaultLayoutField
            key={groupKey}
            fieldKey={groupKey}
            baseData={groupData}
            baseMetaData={groupMetaData}
            fields={fieldList}
            fieldMap={fieldMap}
          />
        );
      })}
    </form>
  );
}

/**
 * Utilities
 */
export type FormBuilderLayoutConfig<FormFields extends FBFormFieldConfigCollection, LayoutFields extends FBLayoutFieldConfigCollection> = {
  groups: {
    [LayoutType in keyof LayoutFields]: {
      key: string;
      type: LayoutFields[LayoutType]['type'];
      data?: Partial<FBLayoutFieldDependencyType<LayoutFields>[LayoutType]['data']>;
      metaData?: Partial<FBLayoutFieldDependencyType<LayoutFields>[LayoutType]['metaData']>;
      dependencies?: CreateDependenciesTypes<FBFormFieldDependencyType<FormFields> & FBLayoutFieldDependencyType<LayoutFields>>;
      fields: {
        [FormType in keyof FormFields]: {
          key: string;
          type: FormFields[FormType]['type'];
          data?: Partial<FBFormFieldDependencyType<FormFields>[FormType]['data']>;
          metaData?: Partial<FBFormFieldDependencyType<FormFields>[FormType]['metaData']>;
          dependencies?: CreateDependenciesTypes<FBFormFieldDependencyType<FormFields> & FBLayoutFieldDependencyType<LayoutFields>>;
        };
      }[keyof FormFields][];
    };
  }[keyof LayoutFields][];
};

export type FormBuilderGroup<
  FormFields extends FBFormFieldConfigCollection,
  LayoutFields extends FBLayoutFieldConfigCollection,
> = FormBuilderLayoutConfig<FormFields, LayoutFields>['groups'][number];

export type FormBuilderField<FormFields extends FBFormFieldConfigCollection, LayoutFields extends FBLayoutFieldConfigCollection> = FormBuilderGroup<
  FormFields,
  LayoutFields
>['fields'][number];

export type FormBuilderLayoutProps<FormFields extends FBFormFieldConfigCollection, LayoutFields extends FBLayoutFieldConfigCollection> = {
  layoutConfig: FormBuilderLayoutConfig<FormFields, LayoutFields>;
};

export { FormBuilderLayout };
